import pandas as pd
import os
import json
import copy
import logging

# function to convert multi level json into single level
def boil_down_array(key, data):
    if type(data) == dict:
        for key, item in data.items():
            yield from boil_down_array(key, item)
    else:
        yield {key:data}

basePath = os.path.dirname(os.path.abspath(__file__))

# log for special cases
logging.basicConfig(filename=basePath + '/ypsolutions_script_log.log', level=logging.DEBUG)

# get unique fields of column desc_meaningful_contact
with open(basePath + '/tapclicks_YPSolutions_events000000000000/tapclicks_YPSolutions_events000000000000.json') as json_file:
    desc_meaningful_contact_unique = []
    line = json_file.readline()
    counter = 0
    while line != None:
        json_line = json.loads(line)
        if json_line.get('desc_meaningful_contact', None) != None:
            if json_line['desc_meaningful_contact'] not in desc_meaningful_contact_unique:
                desc_meaningful_contact_unique.append(json_line['desc_meaningful_contact'])
        line = json_file.readline()
        counter += 1
        if counter == 100:
            break

# create a dictionary out of the unique values
desc_meaningful_contact_unique_dict = dict((el,0) for el in desc_meaningful_contact_unique)

# iterate over the entire file and process 
with open(basePath + '/tapclicks_YPSolutions_events000000000000/tapclicks_YPSolutions_events000000000000.json') as json_file:
    line = json_file.readline()
    counter, master_list, master_count = 0, [], 1
    include_ids = [200137,200139,200298,200300,200302]
    heading_df = pd.read_csv(basePath + '/tapclicks_YPSolutions_events000000000000/Heading.csv',sep='\t')  
    directory_df = pd.read_csv(basePath + '/tapclicks_YPSolutions_events000000000000/OnlineMarket.csv')  
    excluded_directory, excluded_heading = set(), set()
    
    while line != None:
        
        json_line, series_list = json.loads(line), {}

        # if id not in valid ids, ignore
        if eval(json_line['desc_platform_id']) not in include_ids:
            continue

        for key, items in json_line.items():
            for i in boil_down_array(key, items):
                series_list.update(i)

        # add the previously created dict to our record
        series_list.update(desc_meaningful_contact_unique_dict)

        # create a copy of the row. deepcopy is used because both will have same address and changing one will change the other.
        new_series_list = copy.deepcopy(series_list)
        del(new_series_list['lk_directory_heading'])
        
        # calculate the number of rows that'll be created after isolating directory and heading
        new_rows_count = 0
        for directory in json.loads(series_list['lk_directory_heading']):
            for headings in directory['headings']:
                new_rows_count += 1
        
        # create new rows based on lk_directory_heading
        for directory in json.loads(series_list['lk_directory_heading']):
            for headings in directory['headings']:
                new_series_list_2 = new_series_list

                # implementation of step 6
                if new_series_list_2.get('desc_meaningful_contact', None) != None:
                    new_series_list_2[new_series_list_2['desc_meaningful_contact']] = 1 / new_rows_count
                
                # implementation of step 3 to 5 for heading
                if not heading_df[heading_df['HeadingEnglishOnlineNo'] == str(int(headings['heading']))].empty:
                    heading_name = heading_df[heading_df['HeadingEnglishOnlineNo'] == str(int(headings['heading']))].iloc[0]
                    new_series_list_2['Heading_English'] = heading_name['HeadingEnglishOnlineName']
                    new_series_list_2['Heading_French'] = heading_name['HeadingFrenchOnlineName']
                    new_series_list_2['Heading_Master'] = heading_name['HeadingEnglishOnlineName'] + '|' + heading_name['HeadingFrenchOnlineName']
                else:
                    # this condition comes when heading id is not found in mapping 
                    new_series_list_2['Heading_English'] = headings['heading']
                    new_series_list_2['Heading_French'] = headings['heading']
                    new_series_list_2['Heading_Master'] = headings['heading']

                    excluded_heading.add(headings['heading']) 
                
                # implementation of step 3 to 5 for directory
                if directory['directory'] != 'unknown' and not directory_df[directory_df['OnlineMarketNoYpa'] == str(int(directory['directory']))][directory_df['OnlineMarketInd'] == 'Y'].empty:
                    directory_name = directory_df[directory_df['OnlineMarketNoYpa'] == str(int(directory['directory']))][directory_df['OnlineMarketInd'] == 'Y'].iloc[0]
                    new_series_list_2['Directory_Name'] = directory_name['OnlineMarketNameRPT']
                else:
                    # this condition comes when directory id not found in mapping. 
                    new_series_list_2['Directory_Name'] = directory['directory']

                    excluded_directory.add(directory['directory'])

                master_list.append(new_series_list_2)
                
        line = json_file.readline()
        counter += 1

        # only work on the first 100 records
        if counter > 100:
            df = pd.DataFrame(master_list)
            df.to_csv(basePath + '/tapclicks_YPSolutions_events000000000000/tapclicks_YPSolutions_events000000000000_' + str(master_count) + '.csv', index=False)
            counter, master_list = 0, []
            master_count += 1

            # breaking the file on first loop. remove to run on entire file. will create multiple files.
            break

if excluded_directory != set():
    logging.debug('Directory ids excluded: {}'.format(excluded_directory))

if excluded_heading != set():
    logging.debug('Heading ids excluded: {}'.format(excluded_heading))