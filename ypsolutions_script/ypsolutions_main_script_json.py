import pandas as pd
import os
import json
import logging
from collections import OrderedDict
import datetime
import pickle

basePath = os.path.dirname(os.path.abspath(__file__))
directory_df = pd.read_csv(basePath + '/YPSolutions_events/OnlineMarket.csv', encoding='iso-8859-1')
heading_df = pd.read_csv(basePath + '/YPSolutions_events/Heading.csv', encoding='iso-8859-1', sep='\t')  
todays_date = datetime.datetime.now().strftime('%Y-%m-%d')
logging.basicConfig(level=logging.DEBUG)

col_rfp = open('yp_canada_cols.pickle', 'rb')
data = pickle.load(col_rfp)
columns = set(data['columns'])
columns = columns.union(set(['Heading_English','Heading_French','Heading_Master','Directory_Name']))
desc_meaningful_contact_unique = data['desc_meaningful_contact']
columns = columns.union(desc_meaningful_contact_unique)

# function to convert multi level json into single level
def boil_down_array(key, data):
    if type(data) == dict:
        for key, item in data.items():
            yield from boil_down_array(key, item)
    else:
        yield {key:data}

def convert_file(filepath):
    
    # create a dictionary out of the unique values
    desc_meaningful_contact_unique_dict = dict((el,0) for el in desc_meaningful_contact_unique)

    # iterate over the entire file and process 
    with open(filepath) as json_file:
        counter, master_list, master_count = 0, [], 1
        include_ids = [200137,200139,200298,200300,200302]
        
        for line in json_file:
            json_line, series_list = json.loads(line), OrderedDict()

            # if id not in valid ids, ignore
            if eval(json_line['desc_platform_id']) not in include_ids:
                continue

            for key, items in json_line.items():
                for i in boil_down_array(key, items):
                    series_list.update(i)
            
            # add the previously created dict to our record
            series_list.update(desc_meaningful_contact_unique_dict)
            
            # calculate the number of rows that'll be created after isolating directory and heading
            new_rows_count = 0

            try:
                for directory in json.loads(series_list['lk_directory_heading']):
                    for headings in directory['headings']:
                        new_rows_count += 1
            
            # create new rows based on lk_directory_heading
                for directory in json.loads(series_list['lk_directory_heading']):
                    for headings in directory['headings']:

                        # implementation of step 6
                        if series_list.get('desc_meaningful_contact', None) != None:
                            series_list[series_list['desc_meaningful_contact']] = 1 / new_rows_count
                        
                        # implementation of step 3 to 5 for heading
                        if not heading_df[heading_df['HeadingEnglishOnlineNo'] == str(int(headings['heading']))].empty:
                            heading_name = heading_df[heading_df['HeadingEnglishOnlineNo'] == str(int(headings['heading']))].iloc[0]
                            series_list['Heading_English'] = heading_name['HeadingEnglishOnlineName']
                            series_list['Heading_French'] = heading_name['HeadingFrenchOnlineName']
                            series_list['Heading_Master'] = heading_name['HeadingEnglishOnlineName'] + '|' + heading_name['HeadingFrenchOnlineName']
                        else:
                            # this condition comes when heading id is not found in mapping 
                            series_list['Heading_English'] = headings['heading']
                            series_list['Heading_French'] = headings['heading']
                            series_list['Heading_Master'] = headings['heading']
                        
                        # implementation of step 3 to 5 for directory
                        if directory['directory'] != 'unknown' and not directory_df[directory_df['OnlineMarketNoYpa'] == str(int(directory['directory']))][directory_df['OnlineMarketInd'] == 'Y'].empty:
                            directory_name = directory_df[directory_df['OnlineMarketNoYpa'] == str(int(directory['directory']))][directory_df['OnlineMarketInd'] == 'Y'].iloc[0]
                            series_list['Directory_Name'] = directory_name['OnlineMarketNameRPT']
                        else:
                            # this condition comes when directory id not found in mapping. 
                            series_list['Directory_Name'] = directory['directory']

                        master_list.append(series_list)
            except:
                master_list.append(series_list)
                pass
                
            counter += 1

            logging.debug('Records done {} for file {}. Master file count is {}. Master list is at {}'.format(counter, os.path.basename(filepath), master_count, len(master_list)))

            if len(master_list) > 74000:
                df = pd.DataFrame(master_list)
                df.to_csv(filepath[:-5] + '_' + str(master_count) + '_' + todays_date + '.csv', index=False, columns=columns)
                master_count += 1
                master_list = []

        if master_list != []:
            df = pd.DataFrame(master_list)
            df.to_csv(filepath[:-5] + '_' + str(master_count) + '_' + todays_date + '.csv', index=False, columns=columns)

convert_file(basePath + '/YPSolutions_events/tapclicks_YPSolutions_events000000000002.json')