import pandas as pd 
import pickle
import os
import json
from collections import OrderedDict

# function to convert multi level json into single level
def boil_down_array(key, data):
    if type(data) == dict:
        for key, item in data.items():
            yield from boil_down_array(key, item)
    else:
        yield {key:data}

basePath = os.path.dirname(os.path.abspath(__file__))
filepath = basePath + '/YPSolutions_events/tapclicks_YPSolutions_events000000000009.json'

with open(filepath) as json_file:

    counter = 0

    line = json_file.readline()
    while line != None:

        
        json_line, series_list = json.loads(line), {}

        try:
            col_rfp = open('yp_canada_cols.pickle', 'rb')
            data = pickle.load(col_rfp)
            columns = set(data['columns'])
            desc_meaningful_contact = set(data['desc_meaningful_contact'])
        except:
            columns = set()
            desc_meaningful_contact = set()

        col_wfp = open('yp_canada_cols.pickle', 'wb')

        for key, items in json_line.items():
                for i in boil_down_array(key, items):
                    series_list.update(i)
        
        new_columns = columns.union(set(series_list.keys()))

        if series_list.get('desc_meaningful_contact', None) != None:
            desc_meaningful_contact = desc_meaningful_contact.union(set([series_list['desc_meaningful_contact']]))
    
        pickle.dump({'columns':new_columns, 'desc_meaningful_contact':desc_meaningful_contact}, col_wfp)
        counter += 1
        line = json_file.readline()
        print('Records done: '+str(counter))